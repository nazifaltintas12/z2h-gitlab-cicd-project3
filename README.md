# Z2H-GitLab-CI/CD-Project3 Node.js API CI/CD Processes in GitLab (English)


## About the Project
This project focuses on learning the continuous integration and deployment (CI/CD) processes of a Node.js API by applying DevOps principles.

## Advanced Level
Our goal is to explore how we can effectively implement CI/CD processes using AWS Lambda and API Gateway in a secure manner.

### Introduction to Node.js API
- The API is a simple Node.js application providing CRUD features for products:
  - Data processing and management capabilities (Only JSON file, no database).
  - Endpoints designed with RESTful approaches.
- Example Node.js Project: [sample nodejs application](https://gitlab.com/a.yilmaz/basic-nodejs-express-app.git)

## Requirements
- **AWS Services**: AWS EC2, AWS Lambda, AWS API Gateway.
- **GitLab CI**: Integration and configuration of GitLab CI/CD processes.
- **Testing Processes**: As a recommendation, initially create test scenarios with Postman collections for the Node.js API.
- **Branch Structure**:
  - Set up the `develop` branch for automatic integration into the pre-production environment.
  - Set up the `main` branch for integration into the production environment.
- **Minimum Requirements**:
  - Configuration and deployment tasks.
  - Use of JSON file as sample data. Optionally, you can provide database integration.
  - Pre-production operations automatically starting with each push to the `develop` branch.
  - Manual triggering for the production environment. A variable named `VERSION` to be entered manually.
- **GitLab Runner**: Use of the runner provided by GitLab.
- **Accessibility**: Accessibility to API endpoints in the local environment should be maintained after deployment. This can be learned using Postman collections.

## Optional Additions
- **Security with Lambda**: Increase security by processing API requests through Lambda functions and API Gateway.
  - Secure our specified API with a Lambda function and API Gateway (Node.js), only the base URL of the API should change, endpoints like `/api/products/1` should work the same.
- **Functional Test Job**: A functional test job that checks the accessibility and functionality of the API after deployment.

## Conclusion
- At the end of this project, we will see how, when the software team just writes code and uploads it to GitLab, the code moves to pre-production and production servers.
- We will see that even if the server addresses of our APIs running on EC2 change, we can provide a consistent API base URL to the business side.
- We will have seen how secure pipelines are implemented, in the real business world.

Good luck.



#  Z2H-GitLab-CI/CD-Project3 Node.js API için GitLab'da CI/CD Süreçleri (TURKISH)

## Proje Hakkında
Bu proje, DevOps prensiplerini uygulayarak bir Node.js API'nin sürekli entegrasyon ve dağıtım (CI/CD) süreçlerini öğrenmeye odaklanmaktadır.

## İleri Seviye için
Amacımız, AWS Lambda ve API Gateway kullanarak güvenli ve etkin bir şekilde CI/CD süreçlerini nasıl gerçekleştirebileceğimizi keşfetmektir.

### Node.js API Tanıtımı
- API, ürünler için CRUD özellikleri sağlayan basit bir Node.js uygulamasıdır:
  - Veri işleme ve yönetim yetenekleri (Sadece JSON dosyası, veritabanı yok).
  - RESTful yaklaşımla tasarlanmış uç noktalar.
- Örnek Node.js Projesi: [örnek nodejs uygulaması](https://gitlab.com/a.yilmaz/basic-nodejs-express-app.git)

## İstenenler
- **AWS Hizmetleri**: AWS EC2, AWS Lambda, AWS API Gateway.
- **GitLab CI**: GitLab CI/CD süreçlerinin entegrasyonu ve yapılandırılması.
- **Test Süreçleri**: Tavsiye olarak, ilk önce Node.js API için Postman koleksiyonları ile test senaryoları oluşturun.
- **Dal Yapısı**:
  - `develop` dalı, ön üretim ortamına otomatik entegrasyon için ayarlayın.
  - `main` dalı, üretim ortamına entegrasyon için ayarlayın.
- **Minimum Gereklilikler**:
  - Yapılandırma ve dağıtım görevleri.
  - Örnek veri olarak JSON dosyası kullanımı. İsteğe bağlı olarak veritabanı entegrasyonu sağlayabilirsiniz.
  - `develop` dalına her itme işlemi yapıldığında otomatik olarak başlayan ön üretim işlemleri.
  - Üretim ortamı için manuel tetikleme. `VERSION` adında bir değişkenin manuel olarak girilmesi.
- **GitLab Runner**: GitLab tarafından sağlanan runner'ın kullanımı.
- **Erişilebilirlik**: Yerel ortamda erişilen API uç noktalarına, dağıtım sonrasında da erişilebilirlik. Bu konuda Postman koleksiyonlarının kullanımı öğrenilebilir.

## Opsiyonel Eklemeler
- **Lambda ile Güvenlik**: API isteklerini Lambda fonksiyonları ve API Gateway ile işleyerek güvenliği artırma.
  - Belirttiğimiz API'ye bir Lambda fonksiyonu ve API Gateway ile (Node.js) araya girip güvenliğini sağlayın. Sadece API'nin temel URL'si değişecek şekilde, `/api/products/1` gibi uç noktalar aynı çalışmalı.
- **Fonksiyonel Test İşi**: Dağıtım sonrası API'nin erişilebilirliğini ve işlevselliğini kontrol eden fonksiyonel test işi.

## Sonuç
- Bu proje bittiğinde, yazılım ekibinin sadece kod yazıp GitLab'a yüklediği zaman, kodun ön üretim ve üretim sunucularına nasıl geçtiğini göreceğiz.
- EC2 üzerinde çalışan API'lerimizin sunucu adresleri değişse bile işletme tarafına değişmeyen bir API temel URL'si sağlayabileceğimizi göreceğiz.
- Güvenli hale getirilmiş pipeline'ların nasıl gerçekleşeceğini, gerçek iş dünyasındaki şekliyle görmüş olacağız.

Kolay gelsin.
